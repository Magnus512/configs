-- Desactiva plugins
local disabled_built_ins = {
    "netrw",
    "netrwPlugin",
    "netrwSettings",
    "netrwFileHandlers",
    "gzip",
    "zip",
    "zipPlugin",
    "tar",
    "tarPlugin",
    "getscript",
    "getscriptPlugin",
    "vimball",
    "vimballPlugin",
    "2html_plugin",
    "logipat",
    "rrhelper",
    "spellfile_plugin",
    "matchit"
}

------------------- HELPERS -------------------------------
local cmd = vim.cmd  -- to execute Vim commands e.g. cmd('pwd')
local fn = vim.fn    -- to call Vim functions e.g. fn.bufnr()
local g = vim.g      -- a table to access global variables
local opt = vim.opt  -- to set options

local function map(mode, lhs, rhs, opts)
    local options = {noremap = true, silent = true}
    if opts then
        options = vim.tbl_extend("force", options, opts)
    end
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

local sets = {}
--map(sets)
-------------------- Packer --------------------------------
require('plugins')

-- Autocompile plugins when changing the plugins.lua file
cmd 'autocmd BufWritePost plugins.lua PackerCompile'

-------------------- OPTIONS -------------------------------
g.mapleader = " "
-- opt.completeopt = {'menuone', 'noinsert', 'noselect'}  -- Completion options (for deoplete)
opt.expandtab = true                -- Use spaces instead of tabs
opt.hidden = true                   -- Enable background buffers
opt.ignorecase = true               -- Ignore case
opt.joinspaces = false              -- No double spaces with join
opt.list = true                     -- Show some invisible characters
opt.number = true                   -- Show line numbers
opt.relativenumber = true           -- Relative line numbers
opt.scrolloff = 4                   -- Lines of context
opt.shiftround = true               -- Round indent::wqwq
opt.shiftwidth = 4                  -- Size of an indent
opt.sidescrolloff = 8               -- Columns of context
opt.smartcase = true                -- Do not ignore case with capitals
opt.smartindent = true              -- Insert indents automatically
opt.splitbelow = true               -- Put new windows below current
opt.splitright = true               -- Put new windows right of current
opt.tabstop = 4                     -- Number of spaces tabs count for
opt.softtabstop=4
opt.termguicolors = true            -- True color support
opt.undofile = true                 -- Undo file.
-- opt.wildmode = {'list', 'longest'}  -- Command-line completion mode
opt.wrap = false                    -- Disable line wrap
opt.mouse = "a"
opt.showmode = false
opt.hlsearch = true
opt.cmdheight = 1
opt.colorcolumn = "80"
--- Tema
opt.background = "dark" -- or "light" for light mode
cmd([[colorscheme gruvbox]])
g.gruvbox_contrast_dark = "medium"
g.gruvbox_number_column = "dark0_hard"
g.gruvbox_sign_column = "dark0_hard"


-------------------- TREE-SITTER ---------------------------
local ts = require 'nvim-treesitter.configs'
ts.setup {ensure_installed = 'maintained', highlight = {enable = true}}

-------------------- LSP -----------------------------------
local lspfuzzy = require 'lspfuzzy'

require'colorizer'.setup()

require("indent_blankline").setup {
    char = "|",
    buftype_exclude = {"terminal"}
}

local function setup_servers()
  require'lspinstall'.setup()
  local servers = require'lspinstall'.installed_servers()
  for _, server in pairs(servers) do
    require'lspconfig'[server].setup{}
  end
end

setup_servers()

-- Automatically reload after `:LspInstall <server>` so we don't have to restart neovim
require'lspinstall'.post_install_hook = function ()
  setup_servers() -- reload installed servers
  vim.cmd("bufdo e") -- this triggers the FileType autocmd that starts the server
end

g.indent_blankline_space_char = "."

lspfuzzy.setup{}  -- Make the LSP client use FZF instead of the quickfix list

-------------------- MAPPINGS ------------------------------
map('', '<leader>c', '"+y')       -- Copy to clipboard in normal, visual, select and operator modes
map('i', '<C-u>', '<C-g>u<C-u>')  -- Make <C-u> undo-friendly
map('i', '<C-w>', '<C-g>u<C-w>')  -- Make <C-w> undo-friendly

-- <Tab> to navigate the completion menu
map('i', '<S-Tab>', 'pumvisible() ? "\\<C-p>" : "\\<Tab>"', {expr = true})
map('i', '<Tab>', 'pumvisible() ? "\\<C-n>" : "\\<Tab>"', {expr = true})

map('n', '<C-l>', '<cmd>noh<CR>')    -- Clear highlights
map('n', '<leader>o', 'm`o<Esc>``')  -- Insert a newline in normal mode

map('n', '<space>,', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>')
map('n', '<space>.', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>')
map('n', '<space>a', '<cmd>lua vim.lsp.buf.code_action()<CR>')
map('n', '<space>d', '<cmd>lua vim.lsp.buf.definition()<CR>')
map('n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>')
map('n', '<space>h', '<cmd>lua vim.lsp.buf.hover()<CR>')
map('n', '<space>m', '<cmd>lua vim.lsp.buf.rename()<CR>')
map('n', '<space>r', '<cmd>lua vim.ls.buf.references()<CR>')
map('n', '<space>s', '<cmd>lua vim.lsp.buf.document_symbol()<CR>')

--- Telescope
map("n", "<Leader>fw", "<cmd>lua require('telescope.builtin').live_grep()<cr>", sets)
map("n", "<Leader>gt", "<cmd>lua require('telescope.builtin').git_status()<cr>", sets)
map("n", "<Leader>cm", "<cmd>lua require('telescope.builtin').git_commits()<cr>", sets)
map("n", "<Leader>ff", "<cmd>lua require('telescope.builtin').find_files()<cr>", sets)
map("n", "<Leader>gf", "<cmd>lua require('telescope.builtin').git_files()<cr>", sets)
-- map("n", "<Leader>fb", "<cmd>lua require('telescope.builtin').file_browser()<cr>", sets)
map("n", "<Leader>fb", "<cmd>lua require('telescope.builtin').buffers()<cr>", sets)
map("n", "<Leader>fh", "<cmd>lua require('telescope.builtin').help_tags()<cr>", sets)

-------------------- COMMANDS ------------------------------
cmd 'au TextYankPost * lua vim.highlight.on_yank {on_visual = false}'  -- disabled in visual mode:
