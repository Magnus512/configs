local present, lspconfig = pcall(require, "lspconfig")
if not present then
    return
end
local util = require 'lspconfig/util'

lspconfig.pylsp.setup{}
lspconfig.ccls.setup{}
lspconfig.arduino_language_server.setup{
    cmd = { "arduino-language-server", 
    "-cli-config", 
    "~/.arduino15/arduino-cli.yaml" }
}
