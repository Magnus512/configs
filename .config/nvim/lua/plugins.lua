-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
-- vim.cmd [[packadd packer.nvim]]
-- Only if your version of Neovim doesn't have https://github.com/neovim/neovim/pull/12632 merged
-- vim._update_package_paths()

return require('packer').startup(function()
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- LSP
  use {'neovim/nvim-lspconfig',
  config = function()
      require "configs.lspconf"
  end
  }

  -- FZF
  use {
      'ojroques/nvim-lspfuzzy',
      requires = {
          {'junegunn/fzf'},
          {'junegunn/fzf.vim'},  -- to enable preview (optional)
      },
  }

  -- Telescope
  use {'nvim-telescope/telescope.nvim',
  requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}},
  config = function()
      require "configs.telescope"
  end
  }
  
  use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make'}

  -- Treesitter
  use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}

  --Icons
  use 'kyazdani42/nvim-web-devicons'

  -- Completion
  use {'hrsh7th/nvim-compe',
  config = function()
      require "configs.compe"
  end
  }

  -- Tree
  use {'kyazdani42/nvim-tree.lua',
  config = function()
      require "configs.nvimtree"
  end
  }

  -- Autopairs
  use {
      "windwp/nvim-autopairs",
      after = "nvim-compe",
      config = function()
          require "configs.autopairs"
      end
  }

  --[[ Pop Up
  use {
      "nvim-lua/popup.nvim",
      after = "plenary.nvim"
  }]]

  -- Git
  use {
      'lewis6991/gitsigns.nvim',
      requires = {
          'nvim-lua/plenary.nvim'
      },
      config = function()
          require "configs.gitsigns"
      end
  }

  use {
      "tpope/vim-fugitive",
      cmd = {
          "Git"
      }
  }

  -- Extras
  use {
      "siduck76/nvim-base16.lua",
      after = "packer.nvim",
      config = function()
          require "theme"
      end
  }

  use {
      "onsails/lspkind-nvim",
      event = "BufEnter",
      config = function()
          require "configs.lspkind"
      end
  }

  use {
  'glepnir/galaxyline.nvim',
    branch = 'main',
    -- your statusline
    config = function() 
        require "configs.eviline" 
    end,
    -- some optional icons
    requires = {'kyazdani42/nvim-web-devicons', opt = true}
    }

  use 'norcalli/nvim-colorizer.lua'

  -- Lineas
  use 'lukas-reineke/indent-blankline.nvim'

  -- Tema
  use {"ellisonleao/gruvbox.nvim", requires = {"rktjmp/lush.nvim"}}
  use 'kabouzeid/nvim-lspinstall'
end)
