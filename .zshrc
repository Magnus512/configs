# bindkey -v
autoload -Uz compinit
autoload -U colors && colors
zmodload zsh/complist
compinit -iuC
# promptinit
zstyle ':completion:*' menu select
zstyle ':completion:*' rehash true

_comp_options+=(globdots)		# Include hidden files.
export EDITOR="$(command -v nvim)"
export VISUAL="$(command -v nvim)"
# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.zsh/history

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

source ~/.zsh/powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Color en sintaxis
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/.zsh/zsh-history-substring-search/zsh-history-substring-search.zsh
fpath=(~/.zsh/zsh-completions/src $fpath)

# Teclas para busqueda
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down


# Alias
alias ls='ls --color=auto'
alias vim='nvim'
